//
// Created by lukas on 04.05.20.
//

#include <GL/glew.h>
#include <iostream>
#include "geometry.hpp"

void Geometry::add_triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
    this->triangles.push_back(Triangle{a, b, c});
    this->vertices.push_back(a);
    this->vertices.push_back(b);
    this->vertices.push_back(c);
    }

Geometry::Geometry() {
    this->vertices = std::vector<glm::vec3>();
    this->vertices_test = std::vector<GLfloat>();
    this->triangles = std::vector<Triangle>();
}

void Geometry::render_all(){
    // TODO alle dreiecke aus triangles rendern
    glBindVertexArray(this->vertex_array);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);

    glBufferData(GL_ARRAY_BUFFER, this->vertices_test.size(), &this->vertices_test,GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    std::cout << vertices_test.size() << std::endl;

    glDrawArrays(GL_TRIANGLES, 0,vertices_test.size()/3);


}

void Geometry::add_vertice(GLfloat a, GLfloat b, GLfloat c) {
    this->vertices_test.push_back(a);
    this->vertices_test.push_back(b);
    this->vertices_test.push_back(c);
}

void Geometry::init_buffers() {
    glGenVertexArrays(1, &this->vertex_array);
    glGenBuffers(1, &this->vertex_buffer);

    glBindVertexArray(this->vertex_array);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
}

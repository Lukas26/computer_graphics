//
// Created by lukas on 22.04.20.
//

#ifndef GSP1_APPLICATION_HPP
#define GSP1_APPLICATION_HPP


#include <memory>
#include <glm/vec3.hpp>
#include "window.hpp"
#include "geometry.hpp"
#include "shader_loader.hpp"

class Application {
public:
    Application();
    ~Application();
    int run();
private:
    void handle_events();
    void handle_key_events(const SDL_KeyboardEvent& event);
    void render();

    bool running{false};
    std::unique_ptr<Window> window_;
    Geometry geo_;
    Shader_loader  loader_;
};


#endif //GSP1_APPLICATION_HPP

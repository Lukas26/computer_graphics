//
// Created by lukas on 22.04.20.
//

#ifndef GSP1_WINDOW_HPP
#define GSP1_WINDOW_HPP

#include <SDL2/SDL.h>

#include <GL/glew.h>//needs to be included before gl.h
#include <GL/gl.h>


class Window {
public:
    Window();
    void init();
    void clear();
    void swap();
    void set_Background(float red, float green, float blue);
private:
    SDL_Window* main_window_{nullptr};
    SDL_GLContext context{nullptr};
};


#endif //GSP1_WINDOW_HPP

//
// Created by lukas on 22.04.20.
//


#include "window.hpp"


Window::Window(){

}

void Window::clear() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


}

void Window::swap() {
    SDL_GL_SwapWindow(main_window_);
}

void Window::init() {
    // SDL_INIT_VIDEO automatic initialize the Eventsystem
    if(SDL_Init(SDL_INIT_VIDEO) != 0){
        throw "could not init SDL2";
    }

    Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    // TODO later maybe add SDL_Fullscreen here
    main_window_ = SDL_CreateWindow("Computer Graphics", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, flags);

    if(this->main_window_ == nullptr)throw "could not create window";

    // enable depth test
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // create openGL context
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4.5);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    context = SDL_GL_CreateContext(main_window_);

    if(this->context == nullptr) throw "could not create openGl context";

    GLenum err = glewInit();
    if(GLEW_OK != err)throw "could not init glew";

    glClearColor(1.0, 1.0, 1.0, 1.0);



    SDL_ShowWindow(main_window_);
}

void Window::set_Background(float red, float green, float blue){
    glClearColor(red, green, blue, 1.0);
}

//
// Created by lukas on 04.05.20.
//

#ifndef CG1_GEOMETRY_HPP
#define CG1_GEOMETRY_HPP

#include <vector>
#include <glm/vec3.hpp>
#include <GL/gl.h>

struct Triangle{
    glm::vec3 a;
    glm::vec3 b;
    glm::vec3 c;
};

class Geometry {
public:
    Geometry();
    void add_triangle(glm::vec3, glm::vec3, glm::vec3);
    void add_vertice(GLfloat a, GLfloat b, GLfloat c);
    void init_buffers();
    void render_all();
private:
    std::vector<Triangle> triangles;
    std::vector<glm::vec3> vertices;
    std::vector<GLfloat> vertices_test;
    // TODO später eher vertices speichern da man auch andere formen zeichnen kann std::vector<glm::vec3> vertices;
    // TODO  buffer ids hier speichern
    GLuint vertex_array = 0;
    GLuint vertex_buffer = 0;

};


#endif //CG1_GEOMETRY_HPP

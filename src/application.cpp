//
// Created by lukas on 22.04.20.
//

#include <iostream>
#include "application.hpp"

int Application::run() {
    this->running = true;
    //this->geo_.add_triangle(glm::vec3 (400.0f,-100.0f,0.0f),glm::vec3 (750.0f,500.0f,0.0f),glm::vec3 (50.0f,500.0f,0.0f));

    this->geo_.add_vertice(100.0f, 100.0f, 0.0f);
    this->geo_.add_vertice(-100.0f, 100.0f, 0.0f);
    this->geo_.add_vertice(0.0f, 0.0f, 0.0f);

    this->geo_.init_buffers();

    this->loader_.load_shaders();
    // main loop
    while(running){
        this->handle_events();
        window_->clear();
        this->render();
        window_->swap();
    }
    return EXIT_SUCCESS;
}

void Application::handle_events() {
    SDL_Event event;
    while (SDL_PollEvent(&event)){
        switch (event.type) {
            case SDL_QUIT:
                this->running = false;
                break;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                handle_key_events(event.key);
                break;
        }
    }
}

void Application::handle_key_events(const SDL_KeyboardEvent &event) {
    switch (event.keysym.sym){
        // first assignment change background on key press
        case SDLK_r:
            this->window_->set_Background(1.0,0.0,0.0);
            break;
        case SDLK_g:
            this->window_->set_Background(0.0, 1.0, 0.0);
            break;
        case SDLK_b:
            this->window_->set_Background(0.0, 0.0, 1.0);
            break;
        case SDLK_w:
            this->window_->set_Background(1.0, 1.0, 1.0);
            break;
        case SDLK_l :
            this->window_->set_Background(0.0,0.0,0.0);
            break;
        case SDLK_t:
            this->geo_.add_vertice(20.0f, 50.0f, 0.0f);
            this->geo_.add_vertice(30.0f, 100.0f, 0.0f);
            this->geo_.add_vertice(50.0f, 70.0f, 0.0f);
            break;
        case SDLK_q :
            this-> running = false;

    }
}

void Application::render() {
    this->geo_.render_all();
}

Application::Application() {
    window_ = std::make_unique<Window>();
    try {
        this->window_->init();
    }
    catch (const std::exception& e) {
        std::cout << e.what() << std::endl;
    }
    this->loader_ = Shader_loader();



}

Application::~Application() {
    std::cout << "exiting" << std::endl;
    SDL_Quit();
}
//TODO fix render a triangle and not a quader
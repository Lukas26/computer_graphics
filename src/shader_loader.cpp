//
// Created by lukas on 05.05.20.
//

#include <iostream>
#include <fstream>
#include <vector>
#include "shader_loader.hpp"
static void glClearError() {
    while (glGetError() != GL_NO_ERROR);
}

static void glCheckError() {
    while(GLenum error = glGetError()) {
        std::cout << "[OpenGL Error] ( " << error << ")" << std::endl;
    }
}
void Shader_loader::load_shaders() {


    // std::cout<< "vertex:\n" << this->source_vert << std::endl;
    // std::cout<< "fragment:\n" << this->source_frag << std::endl;

    this->vertex = glCreateShader(GL_VERTEX_SHADER);


    const char* source_c_str_vert = source_vert.c_str();
    glShaderSource(vertex, 1, &source_c_str_vert, 0);
    glCompileShader(vertex);

    this->fragment = glCreateShader(GL_FRAGMENT_SHADER);

    const char* source_c_str_frag = source_frag.c_str();
    glShaderSource(fragment, 1, &source_c_str_frag, 0);
    glCompileShader(fragment);

    handle_compile_errors();

    this->programm_ = glCreateProgram();

    glAttachShader(this->programm_, this->vertex);
    glAttachShader(this->programm_, this->fragment);

    glLinkProgram(this->programm_);

    GLint is_linked=0;
    glGetProgramiv(this->programm_, GL_LINK_STATUS, &is_linked);
    if(is_linked == GL_FALSE){
        GLint maxLength = 0;

        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetProgramInfoLog(this->programm_, maxLength, &maxLength, &errorLog[0]);
        std::cout << "linker error: "<< std::endl;
        for(GLchar i: errorLog){
            std::cout << i;
        }
        std::cout << std::endl;
    }
    glUseProgram(this->programm_);
}

void Shader_loader::read_source() {
    auto path = "../shaders/vertex.glsl";
    std::ifstream fileStream(path, std::ios::in);

    if(!fileStream.is_open()) {
        std::cerr << "Could not read file " << path << ". File does not exist." << std::endl;
    }
    std::string line;
    while(!fileStream.eof()) {
        std::getline(fileStream, line);
        this->source_vert.append(line + "\n");
    }

    fileStream.close();

    path = "../shaders/fragment.glsl";
    line = "";
    std::ifstream fs(path, std::ios::in);

    if(!fs.is_open()) {
        std::cerr << "Could not read file " << path << ". File does not exist." << std::endl;

    }
    while(!fs.eof()) {
        std::getline(fs, line);
        this->source_frag.append(line + "\n");
    }
    fs.close();

}

Shader_loader::Shader_loader() {
    this->read_source();

}

void Shader_loader::handle_compile_errors() {
    // error handling if  its not compiled
    GLint isCompiled = 0;
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &maxLength);
        std::cout << "vertex shader error: "<< std::endl;
        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(vertex, maxLength, &maxLength, &errorLog[0]);
        for(GLchar i: errorLog){
            std::cout << i;
        }
        std::cout << std::endl;
        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        return;
    }
    isCompiled = 0;
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &isCompiled);

    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(fragment, maxLength, &maxLength, &errorLog[0]);
        std::cout << "fragment shader error: "<< std::endl;
        for(GLchar i: errorLog){
            std::cout << i;
        }
        std::cout << std::endl;
        return;
    }
}
//
// Created by lukas on 05.05.20.
//

#ifndef CG1_SHADER_LOADER_HPP
#define CG1_SHADER_LOADER_HPP


#include <string>
#include <GL/glew.h>

class Shader_loader {
public:
    Shader_loader();
    void read_source();
    void load_shaders();
    void handle_compile_errors();
private:
    GLuint programm_;
    GLuint vertex, fragment;

    std::string source_vert;
    std::string source_frag;
};


#endif //CG1_SHADER_LOADER_HPP
